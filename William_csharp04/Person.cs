﻿using System;
using System.Collections.Generic;
using System.Text;

namespace William_csharp04
{
    public class Person
    {
        public Person(string name, string address, DateTime dateofbirth, EnumEthnic ethnic)
        {
            Name = name;
            Address = address;
            DateOfBirth = dateofbirth;
            Ethnic = ethnic;
        }

        public string Name { get; set; }
        public string Address { get; set; }

        public DateTime DateOfBirth { get; set; }

        public EnumEthnic Ethnic { get; set; } // untuk kusus di indonesia ya ko.

        public int Age
        {
            get
            {
                int years = DateTime.Now.Year - DateOfBirth.Year;

                if ((DateOfBirth.Month > DateTime.Now.Month) || (DateOfBirth.Month == DateTime.Now.Month && DateOfBirth.Day > DateTime.Now.Day))
                {
                    years--;
                }

                return years;
            }
        }

        public enum EnumEthnic
        {
            Jawa = 0,
            Sunda = 1,
            Batak = 2,
            Betawi = 3
        }
    }
}