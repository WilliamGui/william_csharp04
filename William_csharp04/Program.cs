﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace William_csharp04
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Extention person = new Extention();
            List<Person> persons = new List<Person>();
            persons.Add(new Person("William", "Jakarta", new DateTime(1999, 02, 18), Person.EnumEthnic.Betawi));
            persons.Add(new Person("Budi", "Pekan Baru", new DateTime(1993, 01, 23), Person.EnumEthnic.Jawa));
            persons.Add(new Person("Willy", "Kalimantan", new DateTime(1995, 06, 12), Person.EnumEthnic.Betawi));
            persons.Add(new Person("Selly", "Batam", new DateTime(1996, 06, 16), Person.EnumEthnic.Batak));
            persons.Add(new Person("Susanti", "Batam", new DateTime(1988, 04, 01), Person.EnumEthnic.Betawi));
            persons.Add(new Person("Xena", "Bandung", new DateTime(1978, 08, 06), Person.EnumEthnic.Sunda));
            persons.Add(new Person("Ahmad", "Makasar", new DateTime(1989, 12, 21), Person.EnumEthnic.Sunda));
            persons.Add(new Person("Bagus", "Solo", new DateTime(1986, 9, 16), Person.EnumEthnic.Jawa));
            persons.Add(new Person("Selamet", "Medan", new DateTime(1993, 08, 14), Person.EnumEthnic.Batak));
            persons.Add(new Person("Kwon Ji young", "Surabaya", new DateTime(1991, 10, 11), Person.EnumEthnic.Jawa));
            int value = person.FilterUmurLebihBesar(30);
            //bool value2 = person.FilterEthnic(Ethnic); // ko saya gak nemu cara untuk enum ethnic ini .

            persons = persons.Where(person => person.Age < value).ToList();
            persons = persons.Where(person => person.Ethnic == Person.EnumEthnic.Jawa).ToList();
        }
    }
}